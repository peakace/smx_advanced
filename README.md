# Manual for smx Project

* Enable Transfer Service
	* Create a dataset for transfer service
		* Go to https://console.cloud.google.com/bigquery?project=YOUR_GOOGLE_CLOUD_PROJECT_ID
		* select your project on the left hand side frame
		* click on the button "Create Dataset"
		* enter a name for the dataset
		* click on the button "Create Dataset"
	* Enable Transfer Service
		* Go to https://console.cloud.google.com/bigquery/transfers?project=YOUR_GOOGLE_CLOUD_PROJECT_ID
		* If not enabled yet the Transfer Service API must be enabled
		* ![Enable Transfer Service API](images/enable_transfer_service_api.png)
		* Click on "Create Trnasfer"
		* ![Create Trnasfer](images/create_transfer.png)
		* Select "Google Ads (formerly AdWords)" as source
		* Select a name for the transfer
		* ![Select Google Ads](images/select_google_ads.png)
		* Enter the name of the already created dataset
		* Enter the Google Ads customer id
			* if the account Id is for an MCC account, all child accounts will be transferred into bigquery ( you pay $2.50 per month for each account)
			* if the account isn't for a non-MCC account then only this account will be transferred
		* Click "save"
		* ![Create Trnasfer](images/create_transfer2.png  "Create Transfer")
* Create Ads Script: Transfer Service Augmenter
	* Go to https://ads.google.com/aw/bulk/scripts
	* Create a new script
		* ![Create new Script](images/create_new_script.png)
	* Copy-paste the source code for transfer_service_augmenter.js into Google Ads UI
	* Click on "Advanced APIs" and enable "Bigquery"
		* ![Advanced APIs](images/advanced_apis.png)
	* Fill out the settings
		* BIGQUERY_PROJECT_ID
		* BIGQUERY_DATASET_ID - use the same dataset as for transfer service
	* Save the script
	* Run
		* authorization dialog is shown
		* ![Authorize](images/authorize.png)
		* Authorize the script to access Bigquery
* Create Dictionary Sheet
	* Go to https://docs.google.com/spreadsheets
	* Click on +
		* ![Library](images/library.png)
	* Write "Brand" into A1
		* put your brand keywords into first column
	* Write "Location" into b1
		* put your location keywords into second column
	* Write "Property" into C1
		* put your property keywords into third column
	* Copy the URL ( you will need it to fill the settings of the app script )
* Create App Script: nl_api.js
	* Go to https://script.google.com/home
	* Click on "New Project"
		* ![New App Script](images/new_app_script.png)
	* Click on "open Project"
	* Name the script
	* Select the “main” function in the toolbar  
		* ![Main](images/main.png)
	* Enable advanced APIs
		* Go to Resources > Advanced Google Services..
		* ![advanced_apis2](images/advanced_apis2.png)
		* enable BigQuery API
		* ![advanced_apis3](images/advanced_apis3.png)
		* enable Google Sheets API ( this is neccessary for the dictionary feature to work )
		* ![advanced_apis4](images/advanced_apis4.png)
	* Fill out script settings
		* TRANSFER_SERVICE_ACCOUNT_ID
			* customer id which was used for enabling transfer service
		* ACCOUNT_ID
			* this must be a non-MCC account id
			* if TRANSFER_SERVICE_ACCOUNT_ID is a non-MCC account, it must be set to the same value
			* if TRANSFER_SERVICE_ACCOUNT_ID is a MCC account, it must be a child account of the transfer service account
		* sheet url from previous step
		* MIN_SQ_CLICKS_FOR_TRAINING
			* choose this depending on the size of your account
				* it should be rather as large as possible
				* but not too large
			* default is 100
			* if the script fails to run with the issue "Could not find any search queries". Try to set MIN_SQ_CLICKS_FOR_TRAINING to a lower value." Follow this suggestion.
		* NL_API_KEY
			* Go to https://console.cloud.google.com/apis/credentials?project=YOUR_GOOGLE_CLOUD_PROJECT_ID
			* Click on "Create new credentials"
			* choose "API key"
			* a popup will show your newly created API key
		* TRANSFER_SERVICE_DATASET_ID
			* put the name of the dataset here which contains the contents of transfer service 
		* DATASET_ID
			* create a new dataset in bigquery and use its name here
		* other settings can be set to default values
	* Save
	* Wait until the Transfer Service run is finished
	* Execute the script
	* Authorize the script to access Bigquery and Drive
		* ![Authorize2](images/authorize2.png)
	* wait some (5 to 10) minutes for the script to finish
	* After execution of the script all tables and views should have been created in bigquery








## Datastudio

* Make a copy of the Datastudio https://datastudio.google.com/open/1SPM5opf2QSoVhkWBel9fYbSXu4j6_hk3
* Connect the correctly named data source
* ![DataStudio](images/data_studio.png)
* Position 1: raw_data
* Position 2: missing_keywords
* Position 3: misrouting_queries
* Position 4: prediction
* Position 5: weights
* Position 6: ngram
* Position 7: counts
* Position 8: paused_keyword
* Position 9: dsa_missing_keywords
* Position 10: shopping_missing_keywords
